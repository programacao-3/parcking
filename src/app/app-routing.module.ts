import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListParckingComponent } from './list-parcking/list-parcking.component';
import { RegisterParckingComponent } from './register-parcking/register-parcking.component'

const routes: Routes = [
    { path: 'list', component: ListParckingComponent },
    { path: 'register', component: RegisterParckingComponent },
    { path: '', pathMatch: 'full', redirectTo: "/list" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
