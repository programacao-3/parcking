import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterParckingComponent } from './register-parcking.component';

describe('RegisterParckingComponent', () => {
  let component: RegisterParckingComponent;
  let fixture: ComponentFixture<RegisterParckingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterParckingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterParckingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
