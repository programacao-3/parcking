import { TemplateDefinitionBuilder } from '@angular/compiler/src/render3/view/template';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Parcking } from './model/parcking.model';
import { Saida } from './model/saida.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  parckingForm = this.formBuilder.group({
      veiculoParcking: ['', [Validators.required] ],
      placaParcking: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(7)] ],
      motoristaParcking: ['', [Validators.required] ],
      telefoneParcking: ['', [Validators.required] ]
  })

  allParcking: Parcking [] = []

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
      let list = localStorage.getItem('parcking-list')
      if (list != null) {
        this.allParcking = JSON.parse(list)
      }
  }

  

  save() {
    if (this.parckingForm.valid) {
      
       
        let veiculo = this.parckingForm.value.veiculoParcking
        let placa = this.parckingForm.value.placaParcking
        let motorista = this.parckingForm.value.motoristaParcking
        let telefone = this.parckingForm.value.telefoneParcking
        let entrada = this.parckingForm
        let saida = this.parckingForm
        let total = this.parckingForm
        

        let parcking = new Parcking(veiculo, placa, motorista, telefone)
        this.allParcking.push(parcking)

        localStorage.setItem('parcking-list', JSON.stringify(this.allParcking))

        this.parckingForm.reset()
    } 
  }

  


  
  
}
