import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListParckingComponent } from './list-parcking/list-parcking.component';
import { RegisterParckingComponent } from './register-parcking/register-parcking.component';

@NgModule({
  declarations: [
    AppComponent,
    ListParckingComponent,
    RegisterParckingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
