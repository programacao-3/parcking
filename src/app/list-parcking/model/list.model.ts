export class List {

    public veiculo: string
    public placa: string
    public motorista: string
    public telefone: string
    public entrada?: Date
    public saida?: Date
    public total: number = 0

    constructor(veiculo: string, placa: string, motorista: string, telefone: string) {
        this.veiculo = veiculo
        this.placa = placa
        this.motorista = motorista
        this.telefone = telefone
        this.entrada = new Date
        this.saida
        this.total

    }
}